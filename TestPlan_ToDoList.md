# Test Plan Document
---
### PRODUCT
- **Product Name:** QE todolist
- **Prepared By:** Thashir Deochand
---
### TABLE OF CONTENTS
  - [PROJECT DESCRIPTION](#project-description)
  - [TESTING STRATEGY](#testing-strategy)
  - [TEST CASES](#test-cases)
---
### PROJECT DESCRIPTION
- The todo list application is an application to assist a user to keep track of their tasks.
- The todo list application will serve as a remote way of organising the user’s task.
- The todo list application will allow multiple users to view a single todo list.
---
### TESTING STRATEGY
- The test strategy taken for this product will be based on front end testing, focussing on user acceptance testing and functional testing.
---
### TEST CASES

#### {TDL01}

#### Test Case Name
* ToDoListDockerTest
#### Test Case Description
* Verify that the application is able to successfully deploy in Docker
#### Pre-conditions
* Git required
* Node.js required
* Docker required
#### Post-conditions
* The todo list application must behave as intended when running through docker
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
#### Test Steps
1. Step 1
	* Step Details
		- Open Docker
		- Build Docker image: `docker build -t qe-todolist .`
		- Run Docker image: `docker run -p 8081:8080 -d qe-todolist`
		- Navigate to `http://localhost:8081`
	* Test Data
		- N/A
	* Expected Results
		- The todo list application must open successfully in a browser
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---
#### {TDL02}

#### Test Case Name
* ToDoListUserTest
#### Test Case Description
* Verify that multiple users are able to view the shared todo list
#### Pre-conditions
* todo list application must contain pre-populated data
* One or more users must be viewing the todo list at the time of execution
#### Post-conditions
* Modification/changes to the todo list must appear on all users todo list upon refresh of the browser tab
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
#### Test Steps
1. Step 1
	* Step Details
		- Launch the todo list application
	* Test Data
		- N/A
	* Expected Results
		- The todo list items across all shared users must match
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
2. Step 2
	* Step Details
		- Modify / alter / Delete / add an Item to the todo list
	* Test Data
		- Wash your hands
	* Expected Results
		- The todo list items across all shared users must match upon refresh of the browser tab
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---
#### {TDL03}

#### Test Case Name
* ToDoListRefreshTest
#### Test Case Description
* Verify that upon a refresh of the browser tab all todo list items still persist
#### Pre-conditions
* todo list application must contain pre-populated data
#### Post-conditions
* The browser to contain all items after a successful refresh
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
#### Test Steps
1. Step 1
	* Step Details
		- Launch the todo list application
		- Execute a refresh of the browser tab
	* Test Data
		- N/A
	* Expected Results
		- The browser to contain all items after a successful refresh
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
2. Step 2
	* Step Details
		- Modify / alter / Delete / add item to the todo list
		- Execute a refresh of the browser tab
	* Test Data
		- Wear your mask
	* Expected Results
		- The browser to contain all items after a successful refresh
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---
#### {TDL04}

#### Test Case Name
* ToDoListValidationTest
#### Test Case Description
* Verify that the application does not allow the user to submit empty values to the todo list
#### Pre-conditions
* N/A
#### Post-conditions
* A validation message for empty field must be displayed in application
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
#### Test Steps
1. Step 1
	* Step Details
		- Launch the todo list application
		- Click on the Submit button without filling any input of data
	* Test Data
		- N/A
	* Expected Results
		- A null value validation message must be displayed to the user
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
2. Step 2
	* Step Details
		- Input blank spaces into the submit text area
		- Click on the Submit button
	* Test Data
		- N/A
	* Expected Results
		- A null value validation message must be displayed to the user
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
3. Step 3
	* Step Details
		- Input a Message into the submit text area
		- Click on the Submit button
		- Click on the update button on the previously added item without any input of data in the update text area
	* Test Data
		- Message - Testing
	* Expected Results
		- A null value validation message must be displayed to the user
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
4. Step 4
	* Step Details
		- Input a message into the submit text area
		- Click on the Submit button
		- Input blank spaces into the update text area
		- Click on the update button
	* Test Data
		- message - Wash the car
	* Expected Results
		- A null value validation message must be displayed to the user
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---
#### {TDL05}

#### Test Case Name
* ToDoListAddDataTest
#### Test Case Description
* Verify that the user is able to add a valid item to the todo list
#### Pre-conditions
* N/A
#### Post-conditions
* Item must be successfully added to todo list
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
#### Test Steps
1. Step 1
	* Step Details
		- Launch the todo list application
		- Input a message into the submit text area
		- Click on the Submit button
	* Test Data
		- message - drink coffee
	* Expected Results
		- The added item must successfully be displayed in the todo list
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---
#### {TDL06}

#### Test Case Name
* ToDoListDeleteDataTest
#### Test Case Description
* Verify that the user is able to successfully delete an item from the todo list
#### Pre-conditions
* todo list application must contain pre-populated data
#### Post-conditions
* Deleted item must be successfully removed from the todo list
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
#### Test Steps
1. Step 1
	* Step Details
		- Launch the todo list application
		- Click the delete button on a valid item
	* Test Data
		- N/A
	* Expected Results
		- A confirmation message must be displayed to prompt the user before deleting the item
		- The chosen item must be successfully removed from the todo list
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---
#### {TDL07}

#### Test Case Name
* ToDoListEditDataTest
#### Test Case Description
* Verify that the user is able to successfully edit an item on the todo list
#### Pre-conditions
* todo list application must contain pre-populated data
#### Post-conditions
* Item must be successfully updated on the todo list
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
#### Test Steps
1. Step 1
	* Step Details
		- Launch the todo list application
		- Input a message into a chosen item update text box
		- Click the update button
	* Test Data
		- message - run automation test
	* Expected Results
		- A confirmation message must be displayed to prompt the user before updating the item
		- The chosen item must be successfully updated on the todo list with the correct message
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---