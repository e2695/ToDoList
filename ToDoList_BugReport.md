## Bug Summary 1
* The application allows blank spaces as a line item
### Steps to reproduce
* Enter blank spaces into the submit text area
* Click the submit button
### Expected behaviour
* Application should display a validation message for null entry
* Application should not add item to the todo list
### Actual behaviour
* Application allows the user to add the blanks spaces as a todo list item
---
## Bug Summary 2
* Application allows the user to click on the update button without any text input in the update text area
### Steps to reproduce
* Add a valid item to the todo list
* Click on the update button without entering any value into the update text area on the above added item
### Expected behaviour
* Application should display a validation message for null entry
* Application should not update the line item on the todo list
### Actual behaviour
* Application updates the chosen item to a null value
---
## Bug Summary 3
* Application allows non valid line entries
### Steps to reproduce
* Enter special characters into the submit text area
* Click the submit button
### Expected behaviour
* Application should display a validation message for a non-valid entry
* Application should not add item to the todo list
### Actual behaviour
* Application allows the user to add this non valid entry to the todo list
---
## Bug Summary 4
* Delete function does not prompt the user to confirm the deletion of an item
### Steps to reproduce
* Add a valid item to the todo list
* Click the Delete button to remove the item
### Expected behaviour
* Application should prompt the user and confirm deletion for this item
### Actual behaviour
* Application deletes the item
---
## Bug Summary 5
* Update function does not prompt the user to confirm the update
### Steps to reproduce
* Add a valid item to the todo list
* Input text to update the above added item
* Click the update button
### Expected behaviour
* Application should prompt the user and confirm the update for this item
### Actual behaviour
* Application updates the item
---
## Bug Summary 6
* Application allows duplicate entries
### Steps to reproduce
* Add a valid item to the todo list
* Add the same item again to the todo list
### Expected behaviour
* Application should prompt the user that this item already exists on the todo list
### Actual behaviour
* Application allows the user to add this duplicate line item


