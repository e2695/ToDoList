#### {TDL08}

#### Test Case Name
* ToDoListCordovaTest
#### Test Case Description
* Verify that the application successfully deploys on a mobile device
#### Pre-conditions
* Application to be ported to Cordova
#### Post-conditions
* Application must successfully deploy on a mobile device
#### Date Tested (dd/mm/yyy)
* N/A
#### Test Case Results
* N/A
---
#### Test Steps
1. Step 1
	* Step Details
		- Launch the application on a mobile device
	* Test Data
		- N/A
	* Expected Results
		- Application must successfully launch on a mobile device
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
2. Step 2
	* Step Details
		- Change the orientation of the device
	* Test Data
		- N/A
	* Expected Results
		- Ensure that the application is fully responsive on a mobile device
		- The layout of the application must fit a full screen of the mobile device in both orientations
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
3. Step 3
	* Step Details
		- Add a message as a todo list item
	* Test Data
		- message - He had done everything right. There had been no mistakes throughout the entire process. It had been perfection and he knew it without a doubt, but the results still stared back at him with the fact that he had lost.
	* Expected Results
		- The item must be successfully added to the todo list
		- The item must appear on the screen without any information being cut off
		- Ensure that all items on the todo list is text wrapped if necessary or a scroller is enabled
	* Actual Results
		- N/A
	* Pass / Fail / Not Executed / Suspended
		- N/A
---